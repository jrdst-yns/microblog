<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Eloquent\Model;

return new class extends Migration
{
  public function up()
  {
    Schema::create('posts', function (Blueprint $table) {
      $table->id();
      $table->bigInteger("shared_post_id")->nullable();
      $table->foreignId("user_id");
      $table->string("image")->nullable();
      $table->string("content", 140);
      $table->timestamps();
      $table->softDeletes();
    });
  }
  public function down()
  {
    Schema::dropIfExists('posts');
  }
};
