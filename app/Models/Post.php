<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    use SoftDeletes;

    /**
     * No. of likes
     *
     * @return integer
     */
    public function getNoOfLikesAttribute()
    {
        return $this->likes()->count();
    }
    /**
     * Get if the user liked the post
     *
     * @return void
     */
    public function getLikedByUserAttribute()
    {
        $userId = auth()->user()->id;
        $like = $this->likes()->select("id")->where("user_id", "=", $userId)->first();

        return $like;
    }
    /**
     * Details of shared post
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function sharedPost()
    {
        return $this->belongsTo(Post::class, "shared_post_id");
    }
    /**
     * Comments of post
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments()
    {
        return $this->hasMany(Comment::class);
    }
    /**
     * Owner of post
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, "user_id");
    }
    /**
     * All likes of post
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function likes()
    {
        return  $this->hasMany(LikedPost::class);
    }
    /**
     * Deleting Likes and Comments when post is deleted
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();
        static::deleting(function ($post) {
            $post->likes()->delete();
            $post->comments()->delete();
        });
    }
}
