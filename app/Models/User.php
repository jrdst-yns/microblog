<?php

namespace App\Models;

use Hash;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    protected $fillable = [
        'first_name',
        'last_name',
        'middle_name',
        'username',
        'birthday',
        'email',
        'password',
    ];

    protected $hidden = [
        'password',
        'verification_token',
    ];
    /**
     * Get if Followed By User
     *
     * @return boolean
     */
    public function getIsFollowedByUserAttribute()
    {
        $userId = auth()->user()->id;

        return $this->followers()->where("user_id", $userId)->count() > 0;
    }
    /**
     * No. of followers
     *
     * @return integer
     */
    public function getNoOfFollowersAttribute()
    {
        return $this->followers()->count();
    }
    /**
     * No. of following
     *
     * @return integer
     */
    public function getNoOfFollowingAttribute()
    {
        return $this->followings()->count();
    }
    /**
     * Full Name Attribute
     *
     * @return string
     */
    public function getFullNameAttribute()
    {
        return ucfirst($this->first_name) . ' ' . ucfirst($this->middle_name) . ' ' . ucfirst($this->last_name);
    }
    /**
     * Hashing password before storing in database
     *
     * @param string $value
     * @return void
     */
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }
    /**
     * Posts of user
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function posts()
    {
        return $this->hasMany(Post::class);
    }
    /**
     * Comments of user
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments()
    {
        return $this->hasMany(Comment::class);
    }
    /**
     * Followers of user
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function followers()
    {
        return $this->hasMany(Follower::class, "following_user_id");
    }
    /**
     * Following of user
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function followings()
    {
        return $this->hasMany(Follower::class);
    }
    /**
     * Liked Post of user
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function likes()
    {
        return $this->hasMany(LikedPost::class);
    }
    /**
     * Deleting posts, comments, followers, following & liked post when deleting user
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();
        static::deleting(function ($user) {
            $user->posts()->delete();
            $user->comments()->delete();
            $user->followers()->delete();
            $user->followings()->delete();
            $user->likedPost()->delete();
        });
    }
}
