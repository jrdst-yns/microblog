<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Follower extends Model
{
    /**
     * Is Following User
     *
     * @return boolean
     */
    public function getIsFollowedByUserAttribute()
    {
        $userId = auth()->user()->followings()->where("following_user_id", "=", $this->user->id)->count();

        return $userId > 0;
    }
    /**
     * Follower
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    /**
     * Following
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function userFollow()
    {
        return $this->belongsTo(User::class, "following_user_id");
    }
}
