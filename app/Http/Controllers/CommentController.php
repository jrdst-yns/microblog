<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Models\Comment;
use App\Http\Requests\StoreCommentRequest;
use App\Http\Requests\UpdateCommentRequest;

class CommentController extends Controller
{
    /**
     * Adding built-in middleware on class construction
     */
    public function __construct()
    {
        $this->middleware("can:delete,comment")->only(['edit','delete']);
    }
    /**
     * Store Comment
     *
     * @param StoreCommentRequest $req
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreCommentRequest $req)
    {
        $safe = $req->safe();
        $comment = new Comment();
        $comment->content = $safe->content;
        $comment->post_id = $safe->post_id;
        $comment->user_id = Auth::user()->id;
        $comment->save();

        return back()->with("success", "Comment posted successfully!");
    }
    /**
     * Edit comment
     *
     * @param Comment $comment
     * @return \Illuminate\Contracts\View\View
     */
    public function edit(Comment $comment)
    {
        return view('comments.edit', compact("comment"));
    }
    /**
     * Updating Comment
     *
     * @param UpdateCommentRequest $req
     * @param Comment $comment
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateCommentRequest $req, Comment $comment)
    {
        $safe = $req->safe();
        $comment->content = $safe->content;
        $comment->save();

        return redirect()->route(
            "posts.show",
            [
                "post" => $comment->post_id
            ]
        )->with("success", "Comment Updated!");
    }
    /**
     * Deleting Comment
     *
     * @param Comment $comment
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Comment $comment)
    {
        $comment->delete();

        return back()->with("success", "Comment Deleted!");
    }
}
