<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Models\LikedPost;
use App\Http\Requests\StoreLikedPostRequest;

class LikedPostController extends Controller
{
    /**
     * Liking a post
     *
     * @param StoreLikedPostRequest $req
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreLikedPostRequest $req)
    {
        $safe = $req->safe();
        $count = LikedPost::where("user_id", "=", Auth::user()->id)
        ->where("post_id", "=", $safe->post_id)->count();
        //Checking if you like the post
        if ($count == 0) {
            // Save to the database that you liked the post
            $likedPost = new LikedPost();
            $likedPost->user_id = Auth::user()->id;
            $likedPost->post_id = $safe->post_id;
            $likedPost->save();

            return back()->with("success", "Liked Post!");
        } else {
            // Unlike the post
            $likedPost = LikedPost::where("post_id", "=", $safe->post_id)->where("user_id", "=", Auth::user()->id);
            $likedPost->delete();

            return back()->with("success", "Unliked post!");
        }
    }
    /**
     * Unlike Post
     *
     * @param LikedPost $likedPost
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(LikedPost $like)
    {
        $like->delete();

        return back()->with("success", "Unliked post!");
    }
}
