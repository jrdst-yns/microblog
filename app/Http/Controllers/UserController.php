<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Hash;
use App\Http\Requests\LoginRequest;
use Mail;
use App\Http\Requests\RegisterUserRequest;
use App\Http\Requests\ResendActivationRequest;
use App\Http\Requests\SearchRequest;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\UpdatePasswordRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Models\User;
use App\Mail\UserMail;

class UserController extends Controller
{
    /**
    * Using the built-in middleware of Policy so that only the owner can edit and delete
    **/
    public function __construct()
    {
        $this->middleware("can:delete,user")->only(['edit','delete']);
    }
    /**
     * This is to show the details of the user including his/her posts/follower/following
     *
     * @param User $user
     * @return \Illuminate\Contracts\View\View
     */
    public function show(User $user)
    {
        $owner = ($user->id == Auth::user()->id);
        $posts = $user->posts()->orderBy('created_at', 'desc')->paginate(5);

        return view("users.show", compact("user", "owner", "posts"));
    }
    /**
     * This is to show the details of a single user for editing
     *
     * @param User $user
     * @return \Illuminate\Contracts\View\View
     */
    public function edit(User $user)
    {
        return view("users.edit", compact("user"));
    }
    /**
     * Update the personal details of a user
     *
     * @param UpdateUserRequest $req
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateUserRequest $req, User $user)
    {
        $safe = $req->safe();
        if (!empty($safe->profile_pic)) {
            $path = $safe->profile_pic->store('public/images/users');
            if ($user->profile_pic) {
                Storage::delete($user->profile_pic);
            }
            $user->profile_pic = $path;
        }
        if (isset($req->rem_image)) {
            Storage::delete($user->profile_pic);
            $user->profile_pic = null;
        }
        $user->email = $safe->email;
        $user->username = $safe->username;
        $user->last_name = $safe->last_name;
        $user->first_name = $safe->first_name;
        $user->middle_name = $safe->middle_name;
        $user->birthday = $safe->birthday;
        $user->save();

        return back()->with("details", "User Details Updated!");
    }
    /**
     * Update Password
     *
     * @param UpdatePasswordRequest $req
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updatePassword(UpdatePasswordRequest $req, User $user)
    {
        $validated = $req->safe();
        if (password_verify($validated->old_password, Auth::user()->password)) {
            $user->password = $validated->password;
            $user->save();

            return back()->with("change_pass", "Password Updated!");
        } else {
            return back()->with("old_password", "Wrong password!");
        }
    }
    /**
     * Register user and sending the activation link after registration
     *
     * @param RegisterUserRequest $req
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(RegisterUserRequest $req)
    {
        $safe = $req->safe();
        $user = new User();
        $user->email = $safe->email;
        $user->username = $safe->username;
        $user->last_name = $safe->last_name;
        $user->first_name = $safe->first_name;
        $user->middle_name = $safe->middle_name;
        $user->birthday = $safe->birthday;
        $user->password = $safe->password;
        $token = base64_encode($user->id . "@" . date("Y-m-d H:i:s", strtotime('+4 hours')));
        $user->verification_token = $token;
        $user->save();
        $mailData = [
            "title" => "User Activation",
            "body" => url("activate/" . $token)
        ];
        Mail::to($user->email)->send(new UserMail($mailData));

        return redirect()
        ->route('register.view')
        ->with('success', 'Please check your email address for the confirmation link.');
    }
    /**
     * Activating User
     *
     * @param string $token
     * @return \Illuminate\Contracts\View\View
     */
    public function activation($token)
    {
        $user = User::where("verification_token", $token)->first();
        $msg = "";
        $id = 0;
        if ($user) {
            $tmp_token = explode("@", base64_decode($token));

            $id = $tmp_token[0];
            $exp = $tmp_token[1];
            if (!empty($user->verified_at)) {
                $msg = "Unknown activation code!";
            } else {
                if (date("Y-m-d H:i:s") <= $exp) {
                    $msg = "Account Successfully Activated!";
                    $user->verified = true;
                    $user->verification_token = "";
                    $user->save();
                } else {
                    $msg = "Activation Link Expired!";
                }
            }
        } else {
            $msg = "Unknown activation code!";
        }

        return view('users.activate', compact("msg", "id"));
    }
    /**
     * Resending a valid token to the user
     *
     * @param ResendActivationRequest $req
     * @return \Illuminate\Http\RedirectResponse
     */
    public function resendActivation(ResendActivationRequest $req)
    {
        $safe = $req->safe();
        $user = User::find($safe->id);
        if ($user) {
            $token = base64_encode($user->id . "@" . date("Y-m-d H:i:s", strtotime('+4 hours')));
            $user->verification_token = $token;
            $user->save();
            $mailData = [
                'title' => 'User Activation',
                'body' => 'http://' . Request()->getHttpHost() . "/activate/" . $token
            ];
            Mail::to($user->email)->send(new UserMail($mailData));

            return redirect()
            ->route('register.view')
            ->with('success', 'Please check your email address for the confirmation link.');
        }
    }
    /**
     * Login function
     *
     * @param LoginRequest $req
     * @return \Illuminate\Http\RedirectResponse
     */
    public function login(LoginRequest $req)
    {
        $safe = $req->safe();
        if (
            Auth::attempt(
                array(
                'username' => $safe->username,
                'password' => $safe->password,
                'verified' => 1
                )
            )
        ) {
            return redirect()->route('home');
        } else {
            return back()->with('warning', 'Wrong username/password!')->onlyInput('username');
        }
    }
    /**
     * Logout function
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function logout()
    {
        Auth::logout();
        session()->invalidate();
        session()->regenerateToken();

        return redirect()->route('login');
    }
    /**
     * Undocumented function
     *
     * @param SearchRequest $req
     * @return \Illuminate\Contracts\View\View
     */
    public function search(SearchRequest $req)
    {
        $search = htmlentities($req->safe()->q);
        $users = User::whereRaw(
            '(LOWER(first_name) LIKE ? OR LOWER(middle_name) LIKE ? OR LOWER(last_name) LIKE ?) AND id!=?',
            ["%{$search}%","%{$search}%","%{$search}%",Auth::user()->id]
        )->paginate(5);

        return view("users.search", compact("users", "search"));
    }
}
