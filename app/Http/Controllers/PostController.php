<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Models\Follower;
use App\Models\Post;
use App\Http\Requests\SearchRequest;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\StorePostRequest;
use App\Http\Requests\UpdatePostRequest;

class PostController extends Controller
{
    /**
    * Using the built-in middleware of Policy so that only the owner can edit and delete
    **/
    public function __construct()
    {
        $this->middleware("can:delete,post")->only(['edit','delete']);
    }
    /**
     * Search Users function
     *
     * @param SearchRequest $req
     * @return \Illuminate\Contracts\View\View
     */
    public function search(SearchRequest $req)
    {
        $safe = $req->safe();
        $search = htmlentities(strtolower($safe->q));
        $followings = Follower::where("user_id", "=", Auth::user()->id)->pluck('following_user_id');
        $followings = $followings->merge([Auth::user()->id]);
        $posts = Post::whereIn("user_id", $followings->all())
        ->whereRaw("LOWER(content) LIKE ?", ["%{$search}%"])->orderByDesc("created_at")->paginate(5);

        return view("users.search", compact("posts", "search"));
    }
    /**
     * Showing of posts and followed users posts
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        $followings = Follower::where("user_id", "=", Auth::user()->id)->pluck('following_user_id');
        $followings = $followings->merge([Auth::user()->id]);
        $posts = Post::whereIn("user_id", $followings->all())->orderByDesc("created_at")->paginate(5);

        return view("home", compact("posts"));
    }
    /**
     * Storing post
     *
     * @param StorePostRequest $req
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StorePostRequest $req)
    {
        $post = new Post();
        $safe = $req->safe();
        if (!empty($safe->shared_post_id)) {
            $content = $safe->shared_content;
            $post->shared_post_id = $safe->shared_post_id;
        } else {
            if (!empty($safe->image)) {
                $path = $safe->image->store('public/images/posts');
                $post->image = $path;
            }
            $content = $req->content;
        }
        $post->content = $content;
        $post->user_id = Auth::user()->id;
        $post->save();

        return redirect()->route("home")->with("success", "Posted successfully!");
    }
    /**
     * Fetching the details of a certain post
     *
     * @param Post $post
     * @return \Illuminate\Contracts\View\View
     */
    public function show(Post $post)
    {
        $comments = $post->comments()->orderByDesc("created_at")->paginate(5);

        return view(
            "posts.show",
            compact("post", "comments")
        );
    }
    /**
     * Fetching the details of a certain post for editing
     *
     * @param Post $post
     * @return \Illuminate\Contracts\View\View
     */
    public function edit(Post $post)
    {
        return view('posts.edit', compact("post"));
    }
    /**
     * Updating of post including deleting of old image if replaced
     *
     * @param UpdatePostRequest $req
     * @param Post $post
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdatePostRequest $req, Post $post)
    {
        $safe = $req->safe();
        if (!empty($safe->image)) {
            $path = $safe->image->store('public/images/posts');
            if ($post->image) {
                Storage::delete($post->image);
            }
            $post->image = $path;
        }
        if (isset($safe->rem_image)) {
            Storage::delete($post->image);
            $post->image = null;
        }
        if (isset($safe->rem_post)) {
            $post->shared_post_id = null;
        }
        $post->content = $safe->content;
        $post->save();

        return redirect()->route(
            'posts.show',
            [
                'post' => $post->id
            ]
        )->with("success", "Post Updated!");
    }
    /**
     * Deletion of post
     *
     * @param Post $post
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Post $post)
    {
        $post->delete();

        return redirect()->route("home")->with("success", "Post Deleted!");
    }
}
