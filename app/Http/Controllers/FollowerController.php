<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Models\Follower;
use App\Http\Requests\StoreFollowerRequest;
use App\Models\User;

class FollowerController extends Controller
{
    /**
     * Get followers/following
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\View
     */
    public function follow($id)
    {
        $routeName = request()->route()->getName();
        $name = null;
        // This is checking if you are the owner
        if (Auth::user()->id != $id) {
            $user = User::findOrFail($id);
            $name = $user->first_name;
        }
        /**
         * Get Followers
         */
        if ($routeName == "users.followers") {
            if ($name) {
                $followers = Follower::where("following_user_id", "=", $id)
                    ->where("user_id", "!=", Auth::user()->id)->paginate(5);
            } else {
                $followers = Follower::where("following_user_id", "=", $id)->paginate(5);
            }

            return view("users.follow", compact("followers", "name"));
        } else {
            /**
             * Get Following
             */
            if ($name) {
                $followings = Follower::where("user_id", "=", $id)
                    ->where("following_user_id", "!=", Auth::user()->id)->paginate(5);
            } else {
                $followings = Follower::where("user_id", "=", $id)->paginate(5);
            }

            return view("users.follow", compact("followings", "name"));
        }
    }
    /**
     * Follow User
     *
     * @param StoreFollowerRequest $req
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreFollowerRequest $req)
    {
        $safe = $req->safe();
        $follower = Follower::where([
            ["user_id","=",Auth::user()->id],
            ["following_user_id","=", $safe->following_user_id]
        ]);
        if ($follower->count() === 0) {
            $following = new Follower();
            $following->following_user_id = $safe->following_user_id;
            $following->user_id = Auth::user()->id;
            $following->save();
        } else {
            $follower->delete();
        }

        return back();
    }

    /**
     * Un-follow User function
     *
     * @param StoreFollowerRequest $req
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(StoreFollowerRequest $req)
    {
        $safe = $req->safe();
        $following = Follower::where([
            ["user_id","=",Auth::user()->id],
            ["following_user_id","=", $safe->following_user_id]
        ]);
        $following->delete();

        return back();
    }
}
