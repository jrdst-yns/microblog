<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class DecodeId
{
    public function handle(Request $req, Closure $next)
    {
        if ($req->route("user")) {
            $req->route()->setParameter('user', base64_decode($req->user));
        } elseif ($req->route("post")) {
            $req->route()->setParameter('post', base64_decode($req->post));
        } elseif ($req->route("comment")) {
            $req->route()->setParameter('comment', base64_decode($req->comment));
        }
        return $next($req);
    }
}
