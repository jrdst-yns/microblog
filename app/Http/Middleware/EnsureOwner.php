<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Response;
use App\Models\Post;
use App\Models\Comment;

class EnsureOwner
{
    public function handle(Request $req, Closure $next)
    {
        $user_id = 0;
        if (isset($req->post)) {
            $user_id = $req->post->user_id;
        } elseif (isset($req->user)) {
            $user_id = $req->user->id;
        } elseif (isset($req->comment)) {
            $user_id = $req->comment->user_id;
        }
        if (Auth::user()->id == $user_id) {
            return $next($req);
        } else {
            return new Response(view("pages.unauthorized"));
        }
    }
}
