<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];
        if ($this->has("shared_post_id")) {
            $rules["shared_content"] = "required|max:140";
            $rules["shared_post_id"] = 'required|numeric|exists:posts,id';
        } else {
            $rules["content"] = 'required|max:140';
            $rules["image"] = 'image|mimes:jpg,png,jpeg,gif|max:2048';
        }

        return $rules;
    }
}
