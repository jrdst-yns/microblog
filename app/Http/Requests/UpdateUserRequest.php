<?php

namespace App\Http\Requests;

use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('update', $this->user);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|unique:users,email,' . $this->user->id,
            'username' => 'required|min:5|max:20',
            'last_name' => 'required|max:100',
            'first_name' => 'required|max:100',
            'middle_name' => '',
            'birthday' => 'required|date',
            'profile_pic' => 'image|mimes:jpg,png,jpeg,gif|max:2048'
        ];
    }
}
