<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|unique:users',
            'username' => 'required|min:5|max:20',
            'last_name' => 'required|max:100',
            'first_name' => 'required|max:100',
            'middle_name' => '',
            'birthday' => 'required|date|before:today',
            'password' => 'required|min:8',
            'confirm_password' => 'required|same:password'
        ];
    }
}
