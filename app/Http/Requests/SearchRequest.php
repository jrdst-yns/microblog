<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SearchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "q" => "required|regex:/^[\w\-\s]+$/"
        ];
    }

    public function messages()
    {
        return [
            'required' => 'The search field is required!',
            'regex' => 'Only alphabets/numbers are allowed in search bar.',
        ];
    }
}
