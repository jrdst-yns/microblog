@extends('layouts.dashboard')
@section("title","View Post")
@section("main_content")
<div class="container">
	<div class="row">
		<div class="col">
			<div class="row mt-2">
				<div class="col d-flex align-items-center post">
					<img width="50" height="50" class="rounded-circle me-2 mt-2" 
					src="
					@if($post->user->profile_pic) 
					{{ Storage::url($post->user->profile_pic) }} 
					@else 
					{{ 'https://res.cloudinary.com/dfv2lwp9b/image/upload/v1650258536/user-ph_nyau61.jpg' }}
					@endif" alt="" />
					<div class="poster-container">
						<a class="link-dark name" href="{{ route("users.show",["user"=> $post->user->id]) }}">
							<b>{{ $post->user->full_name; }}</b>
						</a>
						<br>
						<span class="text-secondary fw-light date">
							{{ date("F d, Y h:i a", strtotime($post->created_at)) }}
						</span>
					</div>
				</div>
				@if(Auth::user()->id === $post->user_id)
				<div class="col d-flex align-items-center justify-content-end">
					<div class="btn-group">
						<button type="button" class="btn dropdown-toggle" data-bs-toggle="dropdown"aria-expanded="false">
							<i class="bi bi-three-dots"></i>
						</button>
						<ul class="dropdown-menu dropdown-menu-end">
							<li>
								<a class="dropdown-item" href="{{ route("posts.edit",['post'=> $post->id]) }}">
									Edit
								</a>
							</li>
							<li>
								<hr class="dropdown-divider">
							</li>
							<li>
								<form id="delPostFrm" method="post" action="{{ route("posts.destroy",["post"=> $post->id]) }}">
									@csrf
									@method("DELETE")
									<a role="button" class="dropdown-item delBtn" id="delPostBtn">Delete</a>
								</form>
							</li>
						</ul>
					</div>
				</div>
				@endif
			</div>
			<div class="row justify-content-center">
				<div class="col-11 px-4">
					<div class="row mt-1">
						<div class="col fs-4">{{ $post->content }}</div>
					</div>
					@if($post->image)
					<div class="row mt-4">
						<div class="col">
							<img class="img-fluid" src="{{ Storage::url($post->image) }}" alt="">
						</div>
					</div>
					@endif
					@if($post->shared_post_id>0)
					<!-- Start of shared post  -->
					@if($post->sharedPost)
					<div class="container">
						<div class="row mt-2">
							<div class="col d-flex align-items-center">
								<img width="50" height="50" class="rounded-circle me-2 mt-2" 
								src="
								@if($post->sharedPost->user->profile_pic) 
								{{ Storage::url($post->sharedPost->user->profile_pic) }} 
								@else 
								{{ 'https://res.cloudinary.com/dfv2lwp9b/image/upload/v1650258536/user-ph_nyau61.jpg' }}
								@endif" alt="" />
								<div>
									<a class="link-dark" href="{{ route("users.show",["user"=>$post->sharedPost->user->id]) }}">
										<b>{{ $post->sharedPost->user->full_name; }}</b>
									</a>
									<br>
									<span class="text-secondary fw-light">
										{{ date("F d, Y h:i a", strtotime($post->sharedPost->created_at)) }}
									</span>
								</div>
							</div>
						</div>
						<div class="row mt-1">
							<div class="col fs-5">
								<a class="content" href="{{ route("posts.show",['post'=> $post->sharedPost->id]) }}">
									{{ html_entity_decode($post->sharedPost->content) }}
								</a>
							</div>
						</div>
						@if($post->sharedPost->image)
						<div class="row mt-4">
							<div class="col">
								<img class="img-fluid" src="{{ Storage::url($post->sharedPost->image) }}" alt="">
							</div>
						</div>
						@endif
					</div>
					@else
					<div class="row">
						<div class="col">
							<h6 class="mt-4 text-center text-secondary">404 not found. The shared post has been deleted.
							</h6>
						</div>
					</div>
					@endif
					<!-- end of shared post -->
					@endif
					<div class="row my-4">
						@php
						$btnClass = "btn-outline-success";
						$icn = "hand-thumbs-up";
						$action = route("likes.store");
						$content = "Like";
						if($post->liked_by_user){
							$content = "Liked";
							$btnClass = "btn-success";
							$icn = "hand-thumbs-up-fill";
							$action = route("likes.destroy",["like" => $post->liked_by_user->id]);
						}
						@endphp
						<form class="col" method="post" action="{{ $action }}">
							@csrf
							@if($post->liked_by_user)
							@method("DELETE")
							@else
							<input type="hidden" name="post_id" id="post_id" value="{{ $post->id }}" />
							@endif
							<button type="submit" class="btn {{ $btnClass }}">
								<i class="bi bi-{{$icn}}"></i>
								{{ $content }}
								@if($post->no_of_likes>0)
								{{ $post->no_of_likes }}
								@endif
							</button>
							<button type="button" class="btn btn-secondary my-4 shareBtn" id="share{{$post->id}}">
								<i class="bi bi-share-fill"></i>
								&nbsp;&nbsp;Share
							</button>
						</form>
					</div>
				</div>
			</div>
			<hr>
			@include('flash_message')
			<form method="post" class="mb-4" action="{{ route("comments.store") }}">
				@csrf
				<input type="hidden" name="post_id" value="{{ $post->id }}" />
				<div class="row">
					<div class="col">
						<textarea class="form-control @error("content") {{"is-invalid"}} @enderror"
						placeholder="Leave a comment here" 
						name="content" 
						id="content" rows="4">{{ old("content") }}</textarea>
						<div class="text-danger mt-1">@error("content") {{$message}} @enderror</div>
					</div>
				</div>
				<div class="row">
					<div class="col mt-2">
						<button type="submit" class="btn btn-primary float-end">
							Add Comment
						</button>
					</div>
				</div>
			</form>
			@foreach($comments as $comment)
			<div class="row">
				<div class="col">
					<div class="row mt-2">
						<div class="col d-flex align-items-center">
							<img width="40" height="40" class="rounded-circle me-2 mt-2" src="
							@if($comment->user->profile_pic) 
							{{ Storage::url($comment->user->profile_pic) }} 
							@else 
							{{ 'https://res.cloudinary.com/dfv2lwp9b/image/upload/v1650258536/user-ph_nyau61.jpg' }}
							@endif" alt="" />
							<div style="line-height: 14px;">
								<a class="link-dark comment name" href="{{ route("users.show",["user"=>$post->user->id]) }}">
									<b>{{ $comment->user->full_name; }}</b>
								</a>
								<br>
								<span class="text-secondary fw-light comment date">
									{{ date("F d, Y h:i a", strtotime($comment->created_at)) }}
								</span>
							</div>
						</div>
						@can("update",$comment)
						<div class="col d-flex justify-content-end">
							<div class="btn-group">
								<button type="button" class="btn dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
									<i class="bi bi-three-dots"></i>
								</button>
								<ul class="dropdown-menu dropdown-menu-end">
									<li>
										<a class="dropdown-item"  href="{{ route("comments.edit",['comment'=> $comment->id]) }}">
											Edit
										</a>
									</li>
									<li>
										<hr class="dropdown-divider">
									</li>
									<li>
										<form id="delCommentFrm{{$comment->id}}" 
											method="post" 
											action="{{ route("comments.destroy",["comment"=> $comment->id]) }}">
											@csrf
											@method("DELETE")
											<a role="button" class="dropdown-item delCommentBtn" id="del{{$comment->id}}">
												Delete
											</a>
										</form>
									</li>
								</ul>
							</div>
						</div>
						@endcan
					</div>
					<div class="row mt-2 justify-content-center">
						<div class="col-11 fs-6 ps-3">
							{{ html_entity_decode($comment->content) }}
						</div>
					</div>
				</div>
			</div>
			<hr>
			@endforeach
			<div class="row">
				<div class="col">
					{!! $comments->withQueryString()->links('pagination::bootstrap-5') !!}
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="delPostConfMdl" aria-hidden="true" tabindex="-1">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalToggleLabel">Confirm Delete</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div class="modal-body">
				Are you sure you want to delete post?
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" id="delPostYesBtn">Yes</button>
				<button type="button" class="btn" data-bs-dismiss="modal">No</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="delCommentConfMdl" aria-hidden="true" tabindex="-1">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalToggleLabel">Confirm Delete</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div class="modal-body">
				Are you sure you want to delete comment?
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" id="delCommentYesBtn">Yes</button>
				<button type="button" class="btn" data-bs-dismiss="modal">No</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="shareMdl" aria-hidden="true" tabindex="-1">
	<div class="modal-dialog modal-lg modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Share Post</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div class="modal-body">
				<form id="shareFrm" method="post" action="{{ route('posts.store') }}">
					@csrf
					<input type="hidden" id="shared_post_id" name="shared_post_id" value="{{ $post->id }}" />
					<textarea 
					class="form-control @error('shared_content') {{'is-invalid'}} @enderror"
					name="shared_content" id="shared_content" rows="5">{{ old("shared_content") }}</textarea>
					<div class="invalid-feedback">@error('shared_content') {{$message}} @enderror</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" id="shareBtn">Share</button>
				<button type="button" class="btn" data-bs-dismiss="modal">Cancel</button>
			</div>
		</div>
	</div>
</div>
<script>
	$(document).ready(function() {
		let id = 0;
		const delPostConfMdl = new bootstrap.Modal(document.getElementById('delPostConfMdl'), {
			keyboard: false,
			backdrop: 'static',
		});
		const delCommentConfMdl = new bootstrap.Modal(document.getElementById('delCommentConfMdl'), {
			keyboard: false,
			backdrop: 'static',
		});
		const shareMdl = new bootstrap.Modal(document.getElementById('shareMdl'), {
			keyboard: false,
			backdrop: 'static',
		});
		$("#delPostBtn").click(function(e) {
			delPostConfMdl.show();
		});
		$("#delPostYesBtn").click(function() {
			$("#delPostFrm").submit();
			delPostConfMdl.hide();
		});
		$(".delCommentBtn").click(function(e) {
			id = $(this).attr("id").replace("del", "");
			delCommentConfMdl.show();
		});
		$("#delCommentYesBtn").click(function() {
			$("#delCommentFrm" + id).submit();
			delCommentConfMdl.hide();
		});
		$(".shareBtn").click(function() {
			const shared_post_id = $(this).attr("id").replace("share", "");
			$("#shared_post_id").val(shared_post_id);
			$("#shared_content").removeClass("is-invalid");
			shareMdl.show();
		});
		$("#shareBtn").click(function() {
			$("#shareFrm").submit();
		});
		@error('shared_content')
		shareMdl.show();
		@enderror
	});
</script>
@endsection