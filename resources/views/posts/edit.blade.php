@extends("layouts.dashboard")
@section("title","Edit Post")
@section("main_content")
<br>
<form method="post" 
    action="{{ route("posts.update",["post" => $post->id]) }}" 
    enctype="multipart/form-data" class="container-fluid">
    @csrf
    @method("PATCH")
    <div class="row">
        <div class="col mb-3">
            @include("flash_message")
            <textarea 
            class="form-control @error("content") {{ "is-invalid" }} @enderror" 
            id="content" 
            rows="8" 
            name="content">@if(old("content")){{ old("content") }}@else{{ $post->content }}@endif</textarea>
            <div class="text-danger mt-1">@error("content") {{$message}} @enderror</div>
        </div>
    </div>
    <div class="row">
        <div class="col">
            @if($post->shared_post_id)
            <a>
                <i class="bi bi-image fs-3" onclick="$("#image").click();"></i>
            </a>
            <div class="d-none">
                <input type="file" name="image" id="image" />
            </div>
            @endif
        </div>
        <div class="col pt-4" id="imgCnt">
            @if($post->image)
            <div class="form-check">
                <input class="form-check-input" type="checkbox" value="1" name="rem_image" id="rem_image">
                <label class="form-check-label" for="rem_image">
                    Remove Image
                </label>
            </div>
            <img id="img_preview" name="img_preview" class="img-fluid" src="{{ Storage::url($post->image) }}" alt="img_preview" />
            @else
            <img id="img_preview" name="img_preview" class="img-fluid d-none" src="" alt="img_preview" />
            @endif
            @if($post->shared_post_id>0)
            <div class="row">
                <div class="col">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="1" name="rem_post" id="rem_post">
                        <label class="form-check-label" for="rem_post">
                            Remove Shared Post
                        </label>
                    </div>
                </div>
            </div>
            @endif
            @if($post->shared_post_id>0&&$post->sharedPost)
            <!-- Start of shared post  -->
            <div class="row mt-2">
                <div class="col d-flex align-items-center">
                    <img width="50" height="50" class="rounded-circle me-2 mt-2" 
                    src="
                    @if($post->sharedPost->user->profile_pic) 
                    {{ Storage::url($post->sharedPost->user->profile_pic) }} 
                    @else 
                    {{ "https://res.cloudinary.com/dfv2lwp9b/image/upload/v1650258536/user-ph_nyau61.jpg" }}
                    @endif" alt="" />
                    <div>
                        <a class="link-dark" href="{{ route("users.show",["user"=>$post->sharedPost->user->id]) }}">
                            <b>{{ $post->sharedPost->user->full_name; }}</b>
                        </a>
                        <br>
                        <span class="text-secondary fw-light">
                            {{ date("F d, Y h:i a", strtotime($post->sharedPost->created_at)) }}
                        </span>
                    </div>
                </div>
            </div>
            <div class="row mt-1">
                <div class="col fs-5 ps-5">
                    <a class="content ms-4" href="{{ route("posts.show",["post"=> $post->sharedPost->id]) }}">
                        {{ html_entity_decode($post->sharedPost->content) }}
                    </a>
                </div>
            </div>
            @if($post->sharedPost->image)
            <div class="row mt-4">
                <div class="col">
                    <img class="img-fluid" src="{{ Storage::url($post->sharedPost->image) }}" alt="">
                </div>
            </div>
            @endif
            <!-- end of shared post -->
            @elseif($post->shared_post_id>0&&!$post->sharedPost)
            <div class="row">
                <div class="col">
                    <h6 class="mt-4 text-center text-secondary">404 not found. The shared post has been deleted.</h6>
                </div>
            </div>
            @endif
        </div>
        <div class="col">
            <div class="mt-2 float-end">
                <button type="submit" class="btn btn-success">
                    <i class="bi bi-save"></i>
                    Save
                </button>
                &nbsp;&nbsp;
                <a href="{{ route("posts.show",["post" => $post->id]) }}" class="btn btn-danger">
                    <i class="bi bi-x"></i>
                    Cancel
                </a>
            </div>
        </div>
    </div>
</div>
<div class="position-fixed top-0 start-50 end-0 p-3" style="z-index: 11">
    <div id="msgToast" class="toast" role="alert" aria-live="assertive" aria-atomic="true">
        <div class="toast-header">
            <strong class="me-auto">Microblog</strong>
            <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
        </div>
        <div class="toast-body" id="msgToastCnt"></div>
    </div>
</div>
<script>
    $(document).ready(function(){
        const toast = new bootstrap.Toast(document.getElementById("msgToast"));
        const readURL = (input) => {
            if(input.files && input.files[0]["type"].split("/")[0] === "image"){
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $("#img_preview").attr("src", e.target.result);
                        $("#img_preview").removeClass("d-none");
                    }
                    reader.readAsDataURL(input.files[0]);
                }
                else{
                    $("#img_preview").addClass("d-none");
                }
            }
            else{
                $("#image").val("");
                $("#msgToastCnt").html("Please select an image!");
                toast.show();
            }
        }
        $("#image").change(function(e){
          readURL(this);
      });
    });
</script>
@endsection