
@php
$title = app()->view->getSections()['title']; 
$profileClass = $followersClass = $followingClass = $homeClass = "nav-link mt-4 link-dark";
if(Route::currentRouteName()==='home'){
  $homeClass = "nav-link active mt-4";
}
elseif ($title==='Following') {
  $followingClass = "nav-link active mt-4";
}
elseif ($title==='Followers') {
  $followersClass = "nav-link active mt-4";
}
elseif ($title==='My Profile') {
  $profileClass = "nav-link active mt-4";
}
@endphp
<a href="/" class="d-flex align-items-center mb-3 mb-md-0 me-md-auto link-dark text-decoration-none">
  <span class="fs-3 fw-bold">MicroBlog</span>
</a>
<ul class="nav nav-pills flex-column mb-auto fs-5">
  <li class="nav-item">
    <a href="{{ route("home") }}" class="{{ $homeClass }}">
      <i class="bi bi-house-door-fill"></i>
      &nbsp;
      Home
    </a>
  </li>
  <li class="nav-item">
    <a href="{{ route("users.following",["id" => Auth::user()->id]) }}" class="{{ $followingClass }}">
      <i class="bi bi-bell-fill"></i>
      &nbsp;
      Following
    </a>
  </li>
  <li>
    <a href="{{ route("users.followers",["id" => Auth::user()->id]) }}" class="{{ $followersClass }}">
      <i class="bi bi-people-fill"></i>
      &nbsp;
      Followers
    </a>
  </li>
  <li>
    <a href="{{ route("users.show",["user" => Auth::user()->id]) }}" class="{{ $profileClass }}">
      <i class="bi bi-person-fill"></i>
      &nbsp;
      Profile
    </a>
  </li>
</ul>
<hr>
<div class="dropdown">
  <a class="d-flex align-items-center link-dark text-decoration-none dropdown-toggle post" data-bs-toggle="dropdown" aria-expanded="false" >
    <img src="
    @if(Auth::user()->profile_pic) 
    {{ Storage::url(Auth::user()->profile_pic) }} 
    @else 
    {{ "https://res.cloudinary.com/dfv2lwp9b/image/upload/v1650258536/user-ph_nyau61.jpg" }} 
    @endif" 
    alt="" width="32" height="32" class="rounded-circle me-2">


    <strong class="text-truncate name">{{ Auth::user()->full_name}}</strong>
  </a>
  <ul class="dropdown-menu text-small shadow" aria-labelledby="dropdownUser2">
    <li>
      <form method="post" action="{{ route('logout') }}">
        @csrf
        <button type="submit" class='dropdown-item'>
          <i class="bi bi-box-arrow-in-left"></i>
          &nbsp;&nbsp;Sign Out
        </button>
      </form>
    </li>
  </ul>
</div>