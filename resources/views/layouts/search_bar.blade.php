<div class="container-fluid bg-white pb-4 border-bottom border-2">
	<div class="row">
		<div class="col-12 col-md-6 col-lg-8 fw-bold fs-4 pt-4">
			@php
			$title = app()->view->getSections()["title"];
			@endphp
			@yield("title")
		</div>
		<div class="col-12 col-md-6 col-lg-4">
			<form method="get" action="{{ route("users.search") }}" class="input-group mt-4">
				<input type="text" 
				class="form-control @error("q") {{"is-invalid"}} @enderror" 
				placeholder="Search" name="q" id="q" value="{{ old("q") }}">
				<button type="submit" class="btn btn-secondary" type="button" id="search_btn">
					<i class="bi bi-search"></i>
				</button>
				
			</form>
			<div class="text-danger mt-1">@error("q") {{$message}} @enderror</div>
		</div>
	</div>
</div>