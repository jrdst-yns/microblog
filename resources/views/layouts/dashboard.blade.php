@extends("layouts.main")
@section("content")
<div class="row justify-content-center">
    <div class="col-12 col-md-10 col-lg-10">
        <div class="row">
            <div class="col-12 col-md-3 col-lg-3 d-none d-md-flex d-lg-flex flex-column flex-shrink-0 p-3 bg-white sticky-lg-top" 
            id="sideCnt">
                @include("layouts.side_nav")
            </div>
            <div class="col-12 col-md-9 col-lg-9" 
            style="border-right: 1px solid rgb(214, 214, 214);border-left: 1px solid rgb(214, 214, 214);min-height: 100vh;" 
            id="main_container"> 
                @include("layouts.search_bar") 
                @yield("main_content")
            </div>
        </div>
    </div>
</div>
@endsection