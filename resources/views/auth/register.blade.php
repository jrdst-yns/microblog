@extends("layouts.main")
@section('title', 'Register')
@section('content')
    <div class="row">
        <div class="col">
            <h1 class="text-primary mt-4 mx-4">MicroBlog</h1>
        </div>
    </div>
    <form method="post" action="{{ route('register.post') }}" class="row justify-content-center">
        @csrf
        <div class="col-12 col-md-6 col-lg-6">
            <h2 class="text-center">Sign Up</h2>
            @include('flash_message')
            <div class="row">
                <div class="col">
                    <div class="mt-3">
                        <label for="email" class="form-label required">Email:</label>
                        <input type="email" class="form-control @error('email') {{ 'is-invalid' }} @enderror" id="email"
                            name="email" value="{{ old('email') }}" />
                        <div class="text-danger mt-1">
                            @error('email')
                                {{ $message }}
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="mt-3">
                        <label for="username" class="form-label required">Username:</label>
                        <input type="text" class="form-control @error('username') {{ 'is-invalid' }} @enderror"
                            id="username" name="username" value="{{ old('username') }}" />
                        <div class="text-danger mt-1">
                            @error('username')
                                {{ $message }}
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="mt-3">
                        <label for="last_name" class="form-label required">Last Name:</label>
                        <input type="text"
                            class="form-control 
                    @error('last_name') {{ 'is-invalid' }} @enderror"
                            id="last_name" name="last_name" value="{{ old('last_name') }}" />
                        <div class="text-danger mt-1">
                            @error('last_name')
                                {{ $message }}
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="mt-3">
                        <label for="first_name" class="form-label required">First Name:</label>
                        <input type="text"
                            class="form-control 
                    @error('first_name') {{ 'is-invalid' }} @enderror"
                            id="first_name" name="first_name" value="{{ old('first_name') }}" />
                        <div class="text-danger mt-1">
                            @error('first_name')
                                {{ $message }}
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="mt-3">
                        <label for="middle_name" class="form-label">Middle Name:</label>
                        <input type="text" class="form-control @error('middle_name') {{ 'is-invalid' }} @enderror"
                            id="middle_name" name="middle_name" value="{{ old('middle_name') }}" />
                        <div class="text-danger mt-1">
                            @error('middle_name')
                                {{ $message }}
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="mt-3">
                        <label for="birthdate" class="form-label required">Birthdate:</label>
                        <input type="date" class="form-control @error('birthday') {{ 'is-invalid' }} @enderror"
                            id="birthday" name="birthday" value="{{ old('birthday') }}" />
                        <div class="text-danger mt-1">
                            @error('birthday')
                                {{ $message }}
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="mt-3">
                        <label for="password" class="form-label required">Password:</label>
                        <input type="password" class="form-control @error('password') {{ 'is-invalid' }} @enderror"
                            id="password" name="password" />
                        <div class="text-danger mt-1">
                            @error('password')
                                {{ $message }}
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="mt-3">
                        <label for="confirm_password" class="form-label required">Confirm Password:</label>
                        <input type="password"
                            class="form-control @error('confirm_password') {{ 'is-invalid' }} @enderror"
                            id="confirm_password" name="confirm_password" />
                        <div class="text-danger mt-1">
                            @error('confirm_password')
                                {{ $message }}
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
            <div class="mt-4 text-center">
                <button class="btn btn-primary">
                    Register
                </button>
            </div>
            <div class="mt-2 text-center">
                <a href="{{ route('login') }}">Already registered? Sign In</a>
            </div>
        </div>
    </form>
@endsection
