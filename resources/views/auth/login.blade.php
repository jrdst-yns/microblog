@extends('layouts.main')
@section("title","Login")
@section("content")
<div class="row">
    <div class="col">
        <h1 class="text-primary mt-4 mx-4">MicroBlog</h1>
    </div>
</div>
<form method="post" action="" class="row justify-content-center">
    @csrf
    <div class="col-12 col-md-4 col-lg-4">
        @include("flash_message")
        <div class="mt-3">
            <label for="username" class="form-label">Username:</label>
            <input type="text" class="form-control" id="username" name="username" value="{{ old('username') }}" />
            <div class="text-danger mt-1">@error('username') {{$message}} @enderror</div>
        </div>
        <div class="mt-3">
            <label for="password" class="form-label">Password:</label>
            <input type="password" class="form-control" id="password" name="password" />
            <div class="text-danger mt-1">@error('password') {{$message}} @enderror</div>
        </div>
        <div class="mt-4 d-grid">
            <button class="btn btn-primary">
                Login
            </button>
        </div>
        <div class="mt-2">
            <a href="{{route('register.view')}}">Not a user yet? Create Account</a>
        </div>
    </div>
</form>
@endsection