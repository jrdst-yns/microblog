@extends('layouts.dashboard')
@if ($owner)
    @section('title', 'My Profile')
@else
    @section('title', $user->full_name . ' - Profile')
@endif
@section('main_content')
    <br>
    <div class="contaier-fluid">
        <div class="row">
            <div class="col-12 col-md-4 col-lg-3 d-flex align-items-center">
                <img class="img-fluid rounded-circle px-2 mt-4" src="
				@if ($user->profile_pic) 
					{{ Storage::url($user->profile_pic) }}
				@else 
					{{ 'https://res.cloudinary.com/dfv2lwp9b/image/upload/v1650258536/user-ph_nyau61.jpg' }} 
				@endif
       			" alt="">
            </div>
            <div class="col">
                <div class="row">
                    <div class="col mt-4">
                        <h2>{{ $user->full_name }}</h2>
                        <span class="fs-5">
                            <a href="{{ route('users.following', ['id' => $user->id]) }}">
                                <b>{{ $user->no_of_following }}</b>
                            </a> &nbsp;Following
                            &nbsp;&nbsp;&nbsp;
                            <a href="{{ route('users.followers', ['id' => $user->id]) }}">
                                <b>{{ $user->no_of_followers }}</b>
                            </a> &nbsp;Followers
                        </span>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        @if ($owner)
                            <a href="{{ route('users.edit', ['user' => $user->id]) }}" class="btn btn-primary my-3">
                                <i class="bi bi-pencil-square"></i>
                                &nbsp;Edit Profile
                            </a>
                        @else
                            @if ($user->is_followed_by_user)
                                <form method="post" action="{{ route('unfollow') }}">
                                    @csrf
                                    @method("DELETE")
                                    <input type="hidden" name="following_user_id" value="{{ $user->id }}" />
                                    <button type="submit" class="btn btn-primary my-3"><i
                                            class="bi bi-check-all"></i>&nbsp;&nbsp;Unfollow</button>
                                </form>
                            @else
                                <form method="post" action="{{ route('follow') }}">
                                    @csrf
                                    <input type="hidden" name="following_user_id" value="{{ $user->id }}" />
                                    <button type="submit" class="btn btn-success my-3"><i
                                            class="bi bi-bell-fill"></i>&nbsp;&nbsp;Follow</button>
                                </form>
                            @endif
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col">
                <div class="container">
                    @if ($posts->count() > 0)
                        @foreach ($posts as $post)
                            <div class="row post">
                                <div class="col">
                                    <div class="row mt-4">
                                        <div class="col d-flex align-items-center">
                                            <img width="40" height="40" class="rounded-circle me-3 mt-2"
                                                src="
              									@if ($post->user->profile_pic) 
													{{ Storage::url($post->user->profile_pic) }} 
												@else 
													{{ 'https://res.cloudinary.com/dfv2lwp9b/image/upload/v1650258536/user-ph_nyau61.jpg' }} 
												@endif"
                                                alt="" />
                                            <div class="poster-container">
                                                <a class="link-dark name" href="#">
                                                    <b>{{ $post->user->full_name }}</b>
                                                </a>
                                                <br>
                                                <span class="text-secondary fw-light date">
                                                    {{ date('F d, Y h:i a', strtotime($post->created_at)) }}
                                                </span>
                                            </div>
                                        </div>
                                        @can('update', $post)
                                            <div class="col d-flex align-items-center justify-content-end">
                                                <div class="btn-group">
                                                    <button type="button" class="btn dropdown-toggle" data-bs-toggle="dropdown"
                                                        aria-expanded="false">
                                                        <i class="bi bi-three-dots"></i>
                                                    </button>
                                                    <ul class="dropdown-menu dropdown-menu-end">
                                                        <li>
                                                            <a class="dropdown-item"
                                                                href="{{ route('posts.edit', ['post' => $post->id]) }}">
                                                                Edit
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <hr class="dropdown-divider">
                                                        </li>
                                                        <li>
                                                            <form id="postDelFrm{{ $post->id }}" method="post"
                                                                action="{{ route('posts.destroy', ['post' => $post->id]) }}">
                                                                @csrf
                                                                @method("DELETE")
                                                                <a role="button" class="dropdown-item delBtn"
                                                                    id="del{{ $post->id }}">
                                                                    Delete
                                                                </a>
                                                            </form>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        @endcan
                                    </div>
                                    <div class="row justify-content-center">
                                        <div class="col-11 px-3">
                                            <div class="row mt-1">
                                                <div class="col fs-4">
                                                    <a class="content"
                                                        href="{{ route('posts.show', ['post' => $post->id]) }}">
                                                        {{ html_entity_decode($post->content) }}
                                                    </a>
                                                </div>
                                            </div>
                                            @if ($post->image)
                                                <div class="row mt-4">
                                                    <div class="col">
                                                        <img class="img-fluid"
                                                            src="{{ Storage::url($post->image) }}" alt="">
                                                    </div>
                                                </div>
                                            @endif
                                            @if ($post->shared_post_id > 0)
                                                @if ($post->sharedPost)
                                                    <!-- Start of shared post  -->
                                                    <div class="container">
                                                        <div class="row mt-2">
                                                            <div class="col d-flex align-items-center">
                                                                <img width="50" height="50" class="rounded-circle me-2 mt-2"
                                                                    src="
                @if ($post->sharedPost->user->profile_pic) {{ Storage::url($post->sharedPost->user->profile_pic) }} 
												@else 
												{{ 'https://res.cloudinary.com/dfv2lwp9b/image/upload/v1650258536/user-ph_nyau61.jpg' }} @endif"
                                                                    alt="" />
                                                                <div>
                                                                    <a class="link-dark"
                                                                        href="{{ route('users.show', ['user' => $post->sharedPost->user->id]) }}">
                                                                        <b>{{ $post->sharedPost->user->full_name }}</b>
                                                                    </a>
                                                                    <br>
                                                                    <span class="text-secondary fw-light">
                                                                        {{ date('F d, Y h:i a', strtotime($post->sharedPost->created_at)) }}
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row mt-1">
                                                            <div class="col fs-5">
                                                                <a class="content"
                                                                    href="{{ route('posts.show', ['post' => $post->sharedPost->id]) }}">
                                                                    {{ html_entity_decode($post->sharedPost->content) }}
                                                                </a>
                                                            </div>
                                                        </div>
                                                        @if ($post->sharedPost->image)
                                                            <div class="row mt-4">
                                                                <div class="col">
                                                                    <img class="img-fluid"
                                                                        src="{{ Storage::url($post->sharedPost->image) }}"
                                                                        alt="">
                                                                </div>
                                                            </div>
                                                        @endif
                                                    </div>
                                                    <!-- end of shared post -->
                                                @else
                                                    <div class="row">
                                                        <div class="col">
                                                            <h6 class="mt-4 text-center text-secondary">
                                                                404 not found. The shared post has been deleted.
                                                            </h6>
                                                        </div>
                                                    </div>
                                                @endif
                                            @endif
                                            <div class="row">
                                                <div class="col">
                                                    @php
                                                        $btnClass = 'btn-outline-success';
                                                        $icn = 'hand-thumbs-up';
                                                        $action = route('likes.store');
                                                        $content = 'Like';
                                                        if ($post->liked_by_user) {
                                                            $content = 'Liked';
                                                            $btnClass = 'btn-success';
                                                            $icn = 'hand-thumbs-up-fill';
                                                            $action = route('likes.destroy', ['like' => $post->liked_by_user->id]);
                                                        }
                                                    @endphp
                                                    <form method="post" action="{{ $action }}">
                                                        @csrf
                                                        @if ($post->likes()->where('user_id', Auth::user()->id)->count() > 0)
                                                            @method("DELETE")
                                                        @else
                                                            <input type="hidden" name="post_id" id="post_id"
                                                                value="{{ $post->id }}" />
                                                        @endif
                                                        <button type="submit" class="btn btn-sm {{ $btnClass }} my-4">
                                                            <i class="bi bi-{{ $icn }}"></i>
                                                            {{ $content }}
                                                            @if ($post->no_of_likes > 0)
                                                                {{ $post->no_of_likes }}
                                                            @endif
                                                        </button>
                                                        &nbsp;
                                                        <a href="{{ route('posts.show', ['post' => $post->id]) }}"
                                                            class="btn btn-sm
													btn-primary my-4">
                                                            <i class="bi bi-chat-dots-fill"></i>
                                                            &nbsp;&nbsp;Comment
                                                        </a>
                                                        &nbsp;
                                                        <button type="button" class="btn btn-sm btn-secondary my-4 shareBtn"
                                                            id="share{{ $post->id }}">
                                                            <i class="bi bi-share-fill"></i>
                                                            &nbsp;&nbsp;Share
                                                        </button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                        @endforeach
                        <div class="row">
                            <div class="col">
                                {!! $posts->withQueryString()->links('pagination::bootstrap-5') !!}
                            </div>
                        </div>
                    @else
                        <h2 class="text-center fw-bold">No posts yet!</h2>
                    @endif
                </div>
            </div>
        </div>

    </div>
    <div class="modal fade" id="delConfMdl" aria-hidden="true" tabindex="-1">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalToggleLabel">Confirm Delete</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    Are you sure you want to delete?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" id="delYesBtn">Yes</button>
                    <button type="button" class="btn" data-bs-dismiss="modal">No</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="shareMdl" aria-hidden="true" tabindex="-1">
        <div class="modal-dialog modal-lg modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Share Post</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form id="shareFrm" method="post" action="{{ route('posts.store') }}">
                        @csrf
                        <input type="hidden" id="shared_post_id" name="shared_post_id" />
                        <textarea class="form-control @error('shared_content') {{ 'is-invalid' }} @enderror" name="shared_content"
                            id="shared_content" rows="5">{{ old('shared_content') }}</textarea>
                        <div class="invalid-feedback">
                            @error('shared_content')
                                {{ $message }}
                            @enderror
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" id="shareBtn">Share</button>
                    <button type="button" class="btn" data-bs-dismiss="modal" data-bs-target="#shareMdl"
                        id="canShareMdl">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            let id = 0;
            const delConfMdl = new bootstrap.Modal(document.getElementById('delConfMdl'), {
                keyboard: false,
                backdrop: 'static',
            });
            let shareMdl = new bootstrap.Modal(document.getElementById('shareMdl'), {
                keyboard: false,
                backdrop: 'static',
            });
            $(".delBtn").click(function(e) {
                id = $(this).attr("id").replace("del", "");
                delConfMdl.show();
            });
            $("#delYesBtn").click(function() {
                $("#postDelFrm" + id).submit();
                delConfMdl.hide();
            });
            $(".shareBtn").click(function() {
                const shared_post_id = $(this).attr("id").replace("share", "");
                $("#shared_post_id").val(shared_post_id);
                $("#shared_content").removeClass("is-invalid");
                shareMdl.show();
            });
            $("#shareBtn").click(function() {
                $("#shareFrm").submit();
            });
            @error('shared_content')
                shareMdl.show();
            @enderror
        });
    </script>
@endsection
