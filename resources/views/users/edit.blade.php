@extends('layouts.dashboard')
@section('title', 'Edit Profile')
@section('main_content')
    <br>
    <div class="container-fluid">
        <form method="post" action="{{ route('users.update', ['user' => $user->id]) }}" enctype="multipart/form-data"
            class="row">
            @csrf
            @method('PATCH')
            <div class="col-12 col-md-4 col-lg-3">
                <div class="row">
                    <div class="col">
                        @if ($user->profile_pic)
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="1" name="rem_image" id="rem_image">
                                <label class="form-check-label" for="rem_image">
                                    Remove Image
                                </label>
                            </div>
                            <img id="profile_pic_preview" class="img-fluid rounded mt-4"
                                src="{{ Storage::url($user->profile_pic) }}" alt="profile_pic" />
                        @else
                            <img id="profile_pic_preview"
                                src="{{ 'https://res.cloudinary.com/dfv2lwp9b/image/upload/v1650258536/user-ph_nyau61.jpg' }}"
                                class="img-fluid rounded mt-4" alt="profile_pic" />
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="mt-2">
                            <input class="form-control" type="file" id="profile_pic" name="profile_pic" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-8 col-lg-9">
                <div class="row">
                    <div class="col">
                        <h3>Edit Personal Information</h3>
                    </div>
                </div>
                <br>
                @if ($message = Session::get('details'))
                    <div class="row">
                        <div class="col">
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                <strong>{{ $message }}</strong>
                                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
								</button>
                            </div>
                        </div>
                    </div>
                @endif
                <div class="row mt-2">
                    <div class="col-12 col-md-4 col-lg-4 mb-3">
                        <label class="form-label required" for="last_name">Last Name:</label>
                        <input class="form-control @error('last_name') {{ 'is-invalid' }} @enderror" type="text"
                            id="last_name" name="last_name" value="{{ $user->last_name }}" />
                        <div class="invalid-feedback">
                            @error('last_name')
                                {{ $message }}
                            @enderror
                        </div>
                    </div>
                    <div class="col-12 col-md-4 col-lg-4 mb-3">
                        <label class="form-label required" for="first_name">First Name:</label>
                        <input class="form-control @error('first_name') {{ 'is-invalid' }} @enderror" type="text"
                            id="first_name" name="first_name" value="{{ $user->first_name }}" />
                        <div class="invalid-feedback">
                            @error('first_name')
                                {{ $message }}
                            @enderror
                        </div>
                    </div>
                    <div class="col-12 col-md-4 col-lg-4 mb-3">
                        <label class="form-label" for="middle_name">Middle Name:</label>
                        <input class="form-control @error('middle_name') {{ 'is-invalid' }} @enderror" type="text"
                            id="middle_name" name="middle_name" value="{{ $user->middle_name }}" />
                        <div class="invalid-feedback">
                            @error('middle_name')
                                {{ $message }}
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-md-4 col-lg-4 mb-3">
                        <label class="form-label required" for="email">Email:</label>
                        <input class="form-control @error('email') {{ 'is-invalid' }} @enderror" type="email" id="email"
                            name="email" value="{{ $user->email }}" />
                        <div class="invalid-feedback">
                            @error('email')
                                {{ $message }}
                            @enderror
                        </div>
                    </div>
                    <div class="col-12 col-md-4 col-lg-4 mb-3">
                        <label class="form-label required" for="username">Username:</label>
                        <input class="form-control @error('username') {{ 'is-invalid' }} @enderror" type="text"
                            id="username" name="username" value="{{ $user->username }}" />
                        <div class="invalid-feedback">
                            @error('username')
                                {{ $message }}
                            @enderror
                        </div>
                    </div>
                    <div class="col-12 col-md-4 col-lg-4 mb-3">
                        <label class="form-label required" for="birthday">Birthday:</label>
                        <input class="form-control @error('birthday') {{ 'is-invalid' }} @enderror" type="date"
                            id="birthday" name="birthday" value="{{ $user->birthday }}" />
                        <div class="invalid-feedback">
                            @error('birthday')
                                {{ $message }}
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col d-flex justify-content-end">
                        <button type="submit" class="btn btn-primary">
                            <i class="bi bi-save"></i>
                            &nbsp;Save
                        </button>
                    </div>
                </div>
            </div>
        </form>
        <form method="post" action="{{ route('users.change-password', ['user' => $user->id]) }}" class="row">
            @csrf
            @method("PATCH")
            <div class="col">
                <div class="row">
                    <div class="col">
                        <br>
                        <hr>
                        <br>
                        <h3>Change Password</h3>
                        <br>
                    </div>
                </div>
                @if ($message = Session::get('change_pass'))
                    <div class="row">
                        <div class="col">
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                <strong>{{ $message }}</strong>
                                <button type="button" class="btn-close" data-bs-dismiss="alert"
                                    aria-label="Close"></button>
                            </div>
                        </div>
                    </div>
                @endif
                <div class="row">
                    <div class="col mb-3">
                        <label class="form-label required" for="password_old">Old Password:</label>
                        <input class="form-control @error('old_password') {{ 'is-invalid' }} @enderror" type="password"
                            id="old_password" name="old_password" />
                        <div class="text-danger mt-1">
                            @error('old_password')
                                {{ $message }}
                            @enderror
                        </div>
                    </div>
                    <div class="col mb-3">
                        <label class="form-label required" for="password">New Password:</label>
                        <input class="form-control @error('password') {{ 'is-invalid' }} @enderror" type="password"
                            id="password" name="password" />
                        <div class="invalid-feedback">
                            @error('password')
                                {{ $message }}
                            @enderror
                        </div>
                    </div>
                    <div class="col mb-3">
                        <label class="form-label required" for="confirm_password">Confirm Password:</label>
                        <input class="form-control @error('confirm_password') {{ 'is-invalid' }} @enderror"
                            type="password" id="confirm_password" name="confirm_password" />
                        <div class="invalid-feedback">
                            @error('confirm_password')
                                {{ $message }}
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col d-flex justify-content-end">
                        <button type="submit" class="btn btn-primary">
                            <i class="bi bi-save"></i>
                            &nbsp;Save
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="position-fixed top-0 start-50 end-0 p-3" style="z-index: 11">
        <div id="msgToast" class="toast" role="alert" aria-live="assertive" aria-atomic="true">
            <div class="toast-header">
                <strong class="me-auto">Microblog</strong>
                <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
            </div>
            <div class="toast-body" id="msgToastCnt"></div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            const toast = new bootstrap.Toast(document.getElementById('msgToast'));
            const readURL = (input) => {
                if (input.files && input.files[0]['type'].split('/')[0] === 'image') {
                    if (input.files && input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function(e) {
                            $('#profile_pic_preview').attr('src', e.target.result);
                            $("#profile_pic_preview").removeClass("d-none");
                        }
                        reader.readAsDataURL(input.files[0]);
                    } else {
                        $("#profile_pic_preview").addClass("d-none");
                    }
                } else {
                    $("#image").val("");
                    $("#msgToastCnt").html("Please select an image!");
                    toast.show();
                }
            }
            $("#profile_pic").change(function(e) {
                readURL(this);
            });
        });
    </script>
@endsection
