@extends('layouts.main')
@section('title', 'User Activation')
@section('content')
    <div class="row">
        <div class="col">
            <h1 class="text-primary mt-4 mx-4">MicroBlog</h1>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-12 col-md-3 col-lg-3 text-center">
            @include('flash_message')
            <h1>{{ $msg }}</h1>
            @if ($msg == 'Account Successfully Activated!')
                <p>
                    Thank you for registering on our platform.
                </p>
                <br>
                <a class="btn btn-success mt-2" href="{{ route('login') }}">Login Now!</a>
            @elseif($msg == 'Activation Link Expired!')
                <br>
                <form method="post" action="{{ route('resend-activation') }}">
                    @csrf
                    <input type="hidden" name="id" value="{{ $id }}" />
                    <button class="btn btn-primary" type="submit">
                        Resend Activation Link
                    </button>
                </form>
            @else
                <a class="btn btn-success mt-2" href="{{ route('register.view') }}">Register Here</a>
            @endif
        </div>
    </div>
@endsection
