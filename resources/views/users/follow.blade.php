@extends('layouts.dashboard')
@if (isset($followers) && empty($name))
    @section('title', 'Followers')
@elseif(isset($followers) && !empty($name))
    @section('title', 'Followers | ' . $name)
@elseif(isset($followings) && !empty($name))
    @section('title', 'Following | ' . $name)
@else
    @section('title', 'Following')
@endif
@section('main_content')
    <div class="row">
        <div class="col">
            @if ($errors->any())
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @else
                @include('flash_message')
            @endif
        </div>
    </div>
    @if (isset($followers))
        @if ($followers->count() > 0)
            @foreach ($followers as $follower)
                <div class="row mt-4">
                    <div class="col-12 col-md-3 col-lg-2 px-5 d-flex align-items-center">
                        <img class="img-fluid rounded-circle mt-4"
                            src="
      						@if ($follower->user->profile_pic) {{ Storage::url($follower->user->profile_pic) }} 
							@else 
							{{ 'https://res.cloudinary.com/dfv2lwp9b/image/upload/v1650258536/user-ph_nyau61.jpg' }} @endif"
                            alt="">
                    </div>
                    <div class="col">
                        <div class="row">
                            <div class="col mt-4">
                                <a class="link-dark" href="{{ route('users.show', ['user' => $follower->user->id]) }}">
                                    <h2>
                                        {{ $follower->user->full_name }}
                                    </h2>
                                </a>
                                <span class="fs-5">
                                    <a href="{{ route('users.following', ['id' => $follower->user->id]) }}">
                                        <b>
                                            {{ $follower->user->no_of_following }}
                                        </b>
                                    </a>&nbsp;Following
                                    &nbsp;&nbsp;&nbsp;
                                    <a href="{{ route('users.followers', ['id' => $follower->user->id]) }}">
                                        <b>
                                            {{ $follower->user->no_of_followers }}
                                        </b>
                                    </a>&nbsp;Followers
                                </span>
                            </div>
                        </div>
                        <div class="row mt-3 mb-4">
                            <div class="col">
                                @if ($follower->is_followed_by_user)
                                    <form method="post" action="{{ route('unfollow') }}">
                                        @csrf
                                        @method("DELETE")
                                        <input type="hidden" name="following_user_id"
                                            value="{{ $follower->user->id }}" />
                                        <button type="submit" class="btn btn-primary">
                                            <i class="bi bi-check-all"></i>
                                            &nbsp;&nbsp;Unfollow
                                        </button>
                                    </form>
                                @else
                                    <form method="post" action="{{ route('follow') }}">
                                        @csrf
                                        <input type="hidden" name="following_user_id"
                                            value="{{ $follower->user->id }}" />
                                        <button type="submit" class="btn btn-success">
                                            <i class="bi bi-bell-fill"></i>
                                            &nbsp;&nbsp;Follow
                                        </button>
                                    </form>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
            <hr>
            <div class="row">
                <div class="col">
                    {!! $followers->withQueryString()->links('pagination::bootstrap-5') !!}
                </div>
            </div>
        @else
            <h1 class="fw-bold text-center">No followers yet.</h1>
        @endif
    @else
        @if ($followings->count() > 0)
            @foreach ($followings as $following)
                <div class="row mt-4">
                    <div class="col-12 col-md-3 col-lg-2 px-5 d-flex align-items-center">
                        <img class="img-fluid rounded-circle mt-4" src="
							@if ($following->userFollow->profile_pic) 
							{{ Storage::url($following->userFollow->profile_pic) }}
							@else 
							{{ 'https://res.cloudinary.com/dfv2lwp9b/image/upload/v1650258536/user-ph_nyau61.jpg' }} 
							@endif
							" alt="">
                    </div>
                    <div class="col">
                        <div class="row">
                            <div class="col mt-4">
                                <a class="link-dark"
                                    href="{{ route('users.show', ['user' => $following->userFollow->id]) }}">
                                    <h2>
                                        {{ $following->userFollow->full_name }}
                                    </h2>
                                </a>
                                <span class="fs-5">
                                    <a href="{{ route('users.following', ['id' => $following->userFollow->id]) }}">
                                        <b>
                                            {{ $following->userFollow->no_of_following }}
                                        </b>
                                    </a>&nbsp;Following
                                    &nbsp;&nbsp;&nbsp;
                                    <a href="{{ route('users.followers', ['id' => $following->userFollow->id]) }}">
                                        <b>
                                            {{ $following->userFollow->no_of_followers }}
                                        </b>
                                    </a>&nbsp;Followers
                                </span>
                            </div>
                        </div>
                        <div class="row mt-3 mb-4">
                            <div class="col">
                                <form method="post" action="{{ route('unfollow') }}">
                                    @csrf
                                    @method("DELETE")
                                    <input type="hidden" name="following_user_id"
                                        value="{{ $following->userFollow->id }}" />
                                    <button type="submit" class="btn btn-primary">
                                        <i class="bi bi-check-all"></i>
                                        &nbsp;&nbsp;Unfollow
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
            <hr>
            <div class="row">
                <div class="col">
                    {!! $followings->withQueryString()->links('pagination::bootstrap-5') !!}
                </div>
            </div>
        @else
            <h1 class="fw-bold text-center">Not following anyone yet.</h1>
        @endif
    @endif
@endsection
