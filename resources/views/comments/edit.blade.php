@extends("layouts.dashboard")
@section("title","Edit Comment")
@section("main_content")
<br>
<form method="post" action="{{ route("comments.update",["comment" => $comment->id]) }}" class="container-fluid">
    @csrf
    @method("PATCH")
    <div class="row">
        <div class="col mb-3">
            @include("flash_message")
            <textarea 
            class="form-control @error("content") {{"is-invalid"}} @enderror" 
            id="content" 
            rows="8" 
            name="content">@if(old("content")){{ old("content") }}@else{{ $comment->content }}@endif</textarea>
            <div class="text-danger mt-1">
                @error("content") 
                {{$message}} 
                @enderror
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <div class="mt-2 float-end">
                <a href="{{ url(
                    route("posts.show",
                    ["post" => $comment->post_id]
                    )) }}" class="btn btn-danger">
                    <i class="bi bi-x-lg"></i>
                    &nbsp;&nbsp;Cancel
                </a>
                &nbsp;&nbsp;
                <button type="submit" class="btn btn-success">
                    <i class="bi bi-save"></i>
                    &nbsp;&nbsp;Save
                </button>
            </div>
        </div>
    </div>
</div>
@endsection