<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
  <title>404 Not Found - MicroBlog</title>
</head>
<body class="d-flex justify-content-center align-items-center border" style="height: 100vh;">
  <div class="text-center">
    <h1 class="fw-bold" style="font-size: 70px; ">404 Page Not Found.</h1>
    <br>
    <a class="btn btn-lg btn-danger" href="/">Go Back</a>
  </div>
</body>
</html>