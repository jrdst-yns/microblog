<!DOCTYPE html>
<html>
<head>
    <title>Micoblog</title>
</head>
<body>
    <h1>{{ $mailData['title'] }}</h1>
    <p>Here is the link to activate your account: <a href="{{ $mailData['body'] }}">Click here.</a></p>
    <br><br>
    <p>Thank you.</p>
</body>
</html>