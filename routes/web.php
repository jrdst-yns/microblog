<?php

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\FollowerController;
use App\Http\Controllers\LikedPostController;
use App\Http\Controllers\PostController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get("/register", function () {
    return view("auth.register");
})->name("register.view");

Route::get("/login", function () {
    if (Auth::check()) {
        return redirect()->route("home");
    } else {
        return view("auth.login");
    }
})->name("login");

Route::controller(UserController::class)->group(function () {
    Route::post("/register", "store")->name("register.post");

    Route::get("/activate/{token}", "activation")->name("activation");

    Route::post("/resend-activation", "resendActivation")->name("resend-activation");

    Route::post("/login", "login");
});

Route::middleware(["auth"])->group(function () {
    Route::get("/", [PostController::class,"index"])->name("home");

    Route::controller(UserController::class)->group(function () {
        Route::get("/search/users", "search")->name("users.search");
        Route::patch("/change-password/{user}", "updatePassword")->name("users.change-password");
    });

    Route::resource("users", UserController::class)->only([
        "show","edit","update"
    ]);

    Route::controller(FollowerController::class)->group(function () {
        Route::get("/{id}/followers", "follow")->name("users.followers");
        Route::get("/{id}/following", "follow")->name("users.following");
        Route::post("follow", "store")->name("follow");
        Route::delete("follow", "destroy")->name("unfollow");
    });

    Route::controller(PostController::class)->group(function () {
        Route::get("/search/posts", "search")->name("posts.search");
    });

    Route::resource("posts", PostController::class)->except([
        "index",
        "create"
    ]);



    Route::resource("comments", CommentController::class)->except([
        "index", "create", "show"
    ]);
    Route::resource("likes", LikedPostController::class)->only([
        "store","destroy"
    ]);



    Route::post("/logout", [UserController::class,"logout"])->name("logout");
});
